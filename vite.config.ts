import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'
import path from 'path'
// import styleImport,{AntdResolve} from 'vite-plugin-style-import'
// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    react(),
    // styleImport({//配置Ant自动化按需引入
    //   resolves:[
    //     AntdResolve()
    //   ]
    // })
  ],
  resolve:{
    alias:{//路径别名配置  npm i -D @types/node
      "@":path.resolve(__dirname,'./src')
    }
  }
})
