import {Navigate} from "react-router-dom"
import React,{lazy} from 'react'
import Login from '@/views/Login'
const Home = lazy(()=>import("@/views/Home"))
const Lage1 = lazy(()=>import("@/views/Home/page1"))
const Lage2 = lazy(()=>import("@/views/Home/page2"))
const Lage301 =lazy(()=>import("@/views/Home/page3/page301"))
const Lage302 =lazy(()=>import("@/views/Home/page3/page302"))
const Lage303 =lazy(()=>import("@/views/Home/page3/page303"))
const Lage401 =lazy(()=>import("@/views/Home/page4/page401"))
const Lage402 =lazy(()=>import("@/views/Home/page4/page402"))
const Lage403 =lazy(()=>import("@/views/Home/page4/page403"))
const Lage5 = lazy(()=>import("@/views/Home/page5"))

//添加懒加载模式lazy报错  A component suspended while responding to synchronous input. This will cause the UI to be replaced with a loading indicator. To fix, updates that suspend should be wrapped with startTransition.
// 懒加载模式需要我们添加一个loading组件
const LazyComponentLoading = (Item:JSX.Element)=>(
    <React.Suspense fallback={<div>loading----</div>}>{Item}</React.Suspense>)
const baseRouter = [
    {
        path:'/',
        element:<Navigate to={'/page1'}/>
    },
    {
        path:'/',
        element:<Home/>,
        children:[
            {
                path:'/page1',
                element:LazyComponentLoading(<Lage1/>)
            },
            {
                path:'/page2',
                element:LazyComponentLoading(<Lage2/>)
            },
            {
                path:'/page3/page301',
                element:LazyComponentLoading(<Lage301/>),
            },
            {
                path:'/page3/page302',
                element:LazyComponentLoading(<Lage302/>),
            },
            {
                path:'/page3/page303',
                element:LazyComponentLoading(<Lage303/>),
            },
            {
                path:'/page4/page401',
                element:LazyComponentLoading(<Lage401/>),
            },
            {
                path:'/page4/page402',
                element:LazyComponentLoading(<Lage402/>),
            },
            {
                path:'/page4/page403',
                element:LazyComponentLoading(<Lage403/>),
            },
            {
                path:'/page5',
                element:LazyComponentLoading(<Lage5/>)
            },
        ]
    },
    {
        path:'/login',
        element:<Login/>,
    },
    {
        path:'*',
        element:<Navigate to={'/page1'}/>
    }
]
export default baseRouter