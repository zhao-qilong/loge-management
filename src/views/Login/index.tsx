import { Input ,Space,Button,message } from 'antd';
import { ChangeEvent, useEffect, useState } from 'react';
import initlogin from "./init.ts";
import {CaptchaAPI,LoginAPI} from '../../request/api.ts';
import './login.scss';
import './login.less';
import { useNavigate } from 'react-router-dom';
function Login(){
    useEffect(()=>{
        initlogin();
        window.onresize = ()=>(initlogin())
        capthImg()
    },[])
    let navigateTo = useNavigate()
    const [UsernameValue,setUsernameValue] = useState("")//用户名
    const [PasswordValue,setPasswordValue] = useState("")//密码
    const [CapthValue,setCapthValue] = useState("")//验证码
    const [CapthImg,setCapthImg] = useState("")//验证码图片

    const changeUsername =(e:ChangeEvent<HTMLInputElement>)=>{
        setUsernameValue(e.target.value);
    }
    const changePassword =(e:ChangeEvent<HTMLInputElement>)=>{
        setPasswordValue(e.target.value);
    }
    const changeCapth =(e:ChangeEvent<HTMLInputElement>)=>{
        setCapthValue(e.target.value);
    }
    const onLogin = async ()=>{
        console.log(UsernameValue,PasswordValue,CapthValue)
        if(!UsernameValue.trim() || !PasswordValue.trim()||!CapthValue.trim()){
            message.error('完善内容')
            return
        }else{
           let parme =  await LoginAPI({
                username:UsernameValue,
                password:PasswordValue,
                code:CapthValue,  
                uuid:localStorage.getItem('uuid'),  
            })
            if(parme.code===200){
                // 1、提示登录成功
                message.success("登录成功！")
                // 2、保存token
                localStorage.setItem("lege-react-management-token",parme.token)
                // 3、跳转到/page1
                navigateTo("/page1")
                // 4、删除本地保存中的uuid
                localStorage.removeItem("uuid")
            }
        }
    }
    const capthImg=()=>{
        CaptchaAPI().then((res:CaptchaAPIRes)=>{
            if(res.code === 200){
                setCapthImg('data:image/gif;base64,' + res.img);
                localStorage.setItem('uuid',res.uuid)
            }
        })
    }
    return(
        <div className="loginPage">
            {/* 存放背景 */}
            <canvas id='canvas' style={{display:'block'}}></canvas>
            <div className={'loginBox'+ " loginbox"}>
                {/* 标题部分 */}
                <div className={'title'}>
                    <h1>Allen赵&nbsp;·&nbsp;通用后台系统</h1>
                    <p>Strive Everyday</p>
                </div>
                {/* 表单部分 */}
                <Space direction="vertical" size="middle" style={{ display: 'flex' }}>
                        <Input placeholder="用户名" onChange={changeUsername}/>
                        <Input.Password placeholder="密码" onChange={changePassword}/>
                        <div className='captchaBox'>
                            <Input placeholder="验证码"  onChange={changeCapth}/>
                            <div className='captchaImg' onClick={capthImg}>
                                <img src={CapthImg} alt="" />
                            </div>
                        </div>
                        <Button className='loginBtn' type="primary" block onClick={onLogin}>登录</Button>
                </Space>
            </div>
        </div>
    )
}
export default Login