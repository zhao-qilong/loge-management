export default {
    state:{
        num:60
    },
    actions:{//同步
        add(newsState:{num:number}){
            newsState.num++
        },
        addValue(newsState:{num:number},actions:{type:string,value:number}){
            newsState.num += actions.value
        }
    },
    AsyncActions:{//异步
        addNum(dispstch:Function){
            setTimeout(()=>{
                dispstch({type:'addValue',value:20})
            },2000)
        }
    },
}