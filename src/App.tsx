import { message } from 'antd';
import { useEffect } from 'react'
import {useRoutes, useLocation, useNavigate} from 'react-router-dom'
import router from "./router"

function ToPage1(){
  const navigateTo = useNavigate()
  useEffect(()=>{
    navigateTo("/page1");
    message.warning('你已经登录过了')
  },[])
  return <div></div>
}
function Login2(){
  const navigateTo = useNavigate()
  useEffect(()=>{
    navigateTo("/login");
    message.warning('请登录')
  },[])
  return <div></div>
}

function BrforeRouterEnter(){
  const outlet = useRoutes(router);

  const location = useLocation()
  const token = localStorage.getItem('lege-react-management-token')
  console.log('location.pathname',location.pathname,'token',token)
  if(location.pathname === '/login' && token){
    return <ToPage1/>
  }
  if(location.pathname !== '/login' && !token){
    return <Login2/>
  }
  return outlet
}
function App() {
  return (
    <div className="App">
     <BrforeRouterEnter/>
    </div>
  )
}

export default App
