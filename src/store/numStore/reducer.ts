import List from './index.ts'
const defaultState = {
   ...List.state
}
let reducer = (state = defaultState,actions:{type:string,value:number})=>{
    let newsState = JSON.parse(JSON.stringify(state))
    // const {type,value} = actions
    // console.log('actions',actions)
    switch(actions.type){
        case 'add':
            List.actions.add(newsState)
            break
        case 'addValue':
            List.actions.addValue(newsState,actions)
            break
        default:
            newsState.num++
            break

    }
    return newsState
}



export default reducer