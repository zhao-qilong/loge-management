import { useSelector,useDispatch } from "react-redux"
import {Button} from 'antd'
import asyncAddNum from '../../../store/numStore'
function Page1(){
    const {num} = useSelector((state:RootState)=>(
        {
          num:state.numReducer.num
        }
     ))
    const dispstch = useDispatch()
    const onChangeNum=()=>{
        dispstch({type:'add'})
     }
    const onChangeNum20=()=>{
        dispstch(asyncAddNum.AsyncActions.addNum)
    }
    return(
        <div className="box">
            这里是Page1组件{num}
            <br/>
            <Button onClick={onChangeNum} type="dashed">NUM ++</Button>
            <Button onClick={onChangeNum20} type="dashed">NUM ++ 20异步</Button>
        </div>
    )
}
export default Page1