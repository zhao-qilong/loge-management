import React from 'react'
import ReactDOM from 'react-dom/client'
// 样式引入顺序注意
// 1.初始化样式
import "reset-css"
// 2.UI框架的样式

// 3.全局样式
// import "@/assets/style/global.scss"

// 4.组件的样式
import App from './App'
import {BrowserRouter} from 'react-router-dom'

import { Provider } from 'react-redux'
import store from './store'
ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
  <Provider store={store}>
    <React.StrictMode>
      <BrowserRouter>
        <App />
      </BrowserRouter>
    </React.StrictMode>,
  </Provider>

)
