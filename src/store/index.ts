import { createStore ,combineReducers,compose,applyMiddleware} from "redux";
import numReducer from './numStore/reducer.ts'
import reduxThunk from "redux-thunk"

const reducers = combineReducers(
    {
        numReducer
    }
)
let composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({}):compose //rt
const store = createStore(reducers,composeEnhancers(applyMiddleware(reduxThunk)))
export default store