// 使用 layout报错 Internal server error: Failed to resolve import "E:/react/loge-management/node_modules/antd/es/theme/style/index" from "src\views\Home\index.tsx". Does the file exist?
// 可能是 layout 版本过高导致，降低版本使用x4版本
  import { Breadcrumb, Layout} from 'antd';
  import React, { useState } from 'react';
  import './index.scss'
  import {Outlet} from 'react-router-dom'
  import MenuList from '@/components/Menu';
  const { Header, Content, Footer, Sider } = Layout;
  
  

  const Views: React.FC = () => {
    const [collapsed, setCollapsed] = useState(false);

    return (
      <Layout style={{ minHeight: '100vh' }}>
        {/* 左侧侧边栏 */}
        <Sider collapsible collapsed={collapsed} onCollapse={value => setCollapsed(value)}>
          <div className="logo" />
            <MenuList/>
        </Sider>
        {/* 右侧内容 */}
        <Layout className="site-layout">
            {/* 头部 */}
            <Header className="site-layout-background" style={{ padding: '16px' }}>
                <Breadcrumb style={{ lineHeight:'32px', }}>
                <Breadcrumb.Item>User</Breadcrumb.Item>
                <Breadcrumb.Item>Bill</Breadcrumb.Item>
                </Breadcrumb>
            </Header>
          {/* 内容 */}
          <Content style={{ margin: '16px 16px 0' }}  className="site-layout-background">
            <Outlet/>
          </Content>
          {/* 底部 */}
          <Footer style={{ textAlign: 'center',padding:0, lineHeight:'48px' }}>Ant Design ©2018 Created by Ant UED</Footer>
        </Layout>
      </Layout>
    );
  };
  
  export default Views;